# jaque_me_BED

## Prueba para BED con jaque.me

### Dirigido a:

+ Luis Escobar, Gerente de Recursos Humanos
+ Juan Carlos López López, Líder de Proyecto
+ Tabata Jennifer Becerril Fuentes

### Lógica y análisis de cada solución

#### Ejercicio 1

Basandome en el ejemplo del documento, la función recibiría un arreglo [3,2,5,9,1,3]
y tiene que devolver [2,5,9] como el subarreglo más grande que se podría obtener de
números ordenados de forma ascendente.

Para este caso decidí que podría recorrer el arreglo y revisar si existía una secuencia
entre cada valor, sin embargo para garantizar que se formara el subarreglo lo que establecí
fue un array temporal donde se alojaban los números que cumplieran con la secuancia ascendente.
Además establecí un array que alojara todos los subarreglos que se formaban durante el recorrido del array principal, con esto sólo tendría que ordenar el array compuesto por subarreglos considerando la longitud de cada uno de los subarrays que alojaba como parámetro a ordenar.

Veamos lo siguiente:
El arreglo principal es [3,2,5,9,1,3]
1) Tomo el primer elemento 3, y lo inserto en un array temporal [3]
2) Evaluo ¿3 es mayor que el siguiente número: 2 o el index del 3 es igual a la longitud del arreglo -1?
    2.1) En caso de ser afirmativo cualquiera de las dos opciones, entonces se guarda el array temporal en el array de resultados y se limpia el array temporal
3) Se pasa al siguiente número y se repite el ciclo hasta terminar de recorrer el array principal.
4) Al finalizar se ordena el array de resultados según la longitud de cada subarray y de forma ascendente y se regresa el último subarray del array resultados, el cual es el de mayor tamaño.

> Tiempo que dura la ejecución de la función: 1.174 ms a 1.618 ms


### Ejercicio 2

En este caso es una sumatoria de n a m. Según la sumatoria de Gauss si sumamos los primeros 100 números nos da un valor M. 
Pero si colocamos la serie al revés dará el mismo resultado
1+2+3+...+99+100 = M
100+99+98+...+2+1 = M
Si aplicamos suma de ecuaciones entonces obtenemos que:
101 + 101 + ... + 101 = 2M
Y de forma reducida obtenemos que 100 veces (101) es igual a 2 veces la suma del 1 al 100, es decir:
100( 100 + 1) = 2M

De aquí sale una fórmula general m ( m +1 ) / 2 = M | m= al número final al que se quiere llegar en la sumatoria.
Sin embargo en este ejercicio recibimos tanto inicio como fin de la sumatoria, por lo que la ecuación se vería modificada a: (m-n+1) (m+n) / 2 = Resultado
Donde: 
    (m - n + 1) => consideraría al valor inicial para la sumatoria
    (m + n) => la suma de los valores opuestos, por ejemplo en Gauss era 100 + 1, 99 +2, etc.

Es así como la función se redujo a suma, resta, multiplicación y división.

> Tiempo que dura la ejecución de la función: 0.315 a 1.315 ms

### Ejercicio 3

En este ejercicio se tiene que obtener el k-ésimo elemento más grande de un array dado.
Por ejemplo [7,2,5,9,8,3] con k=2.
El resultado debería ser 8.
¿Qué pasa para obtener ese resultado?
El número k=2 nos dice que quiere el penúltimo elemento más grande del array. Si este fuera 3 pediría el antepenúltimo elemento.
En otras palabras, segun k se debe ordenar el arreglo de mayor a menor y obtener el k elemento de esa secuencia iniciando por el elemento más grande. Entonces k nos da el índice donde se encuentra ese valor, pero como los arreglos inician en 0 (cero) se debe restar 1 al valor de k.

> Tiempo que dura la ejecución de la función: 0.315 a 1.31 ms

### Ejercicio 4

Este ejercicio recibe un array de números con valores duplicados, el cual debe limpiarse para que no tenga duplicados.
Para este caso lo que se hizo fue considerar que si el dígito fuera único entonces el resultado de la función indexOf(valor a evaluar) debería ser igual al primer index que se encuentre al recorrer el array.
Por lo que si filtramos el array con aquellos números que cumplen la condición anterior entonces obtendríamos un array sin valores repetidos.

> Tiempo que dura la ejecución de la función: 0.26 a 1.34 ms

### Ejercicio 5

Para este caso, se considera que la función recibe un String con paréntesis, corchetes y llaves. 
Una condición a considerar es que dicho String tendría una longitud divisible entre 2.
Por lo cual se puede inferir que si de primer instancia el String no tiene una longitud divisible entre 2 entonces la función regresa `false`.
Para los demás casos donde la longitud del String es divisible entre 2 entonces se tiene que verificar que
el primer elemento debe ser apertura del último elemento, entonces una opción sería tener 2 arrays.
El primero con la primer mitad del array original y el segundo con la otra mitad.
Si aplicamos reverse a la segunda mitad, será más simple recorrer los dos arrays, y aplicando Object-Literals podríamos validar que el paréntesis de apertura le corresponde su paréntesis de cierre.

> Tiempo que dura la ejecución de la función: 1.6 a 1.8 ms