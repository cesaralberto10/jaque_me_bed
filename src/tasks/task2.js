/*
*   Exercise #2
*   Description:
*   Escribir una función que reciba 2 enteros no negativos
*   n y m y obtenga la sumatoria de n a m.
*   author: César Alberto Trejo Juárez
*
*/
const summation = (start, end) => {
    // la sumatoria de 1 a m = m(m+1) / 2
    // la sumatoria de n a m = (m-n+1)(m+n) / 2
    return (end - start + 1)*(end + start)/2
};

module.exports = {
    summation: summation
};