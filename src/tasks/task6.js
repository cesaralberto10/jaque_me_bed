/*
*   Exercise #6
*   Description:
*   Tienes un conjunto de clases que tienen una hora de inicio
*   y una de final, hay que acomodarlas en el menor número de
*   salones sin que se traslapan, una clase de 11:00 a 14:00 no
*   puede estar en el mismo salón que una de 12:00 a 15:00.
*   Escribe una función que se encuentre ese número de salones,
*   por ejemplo, si tienes clases de 11:00 a 14:00, 12:00 a 15:00
*   y 14:30 a 16:00 el menor número de salones es 2.
*   author: César Alberto Trejo Juárez
*
*/

const findMinClassrooms = (scheduleList) => {
    // code here
    // let tempRoom = [];
    // let rooms = [];
    // let descending = scheduleList.sort((a,b) => a.end - b.end);
    // let ascending = scheduleList.sort((a,b) => a.start - b.start);
    // return rooms.length;
};

module.exports = {
    findMinClassrooms: findMinClassrooms
};