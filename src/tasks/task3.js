/*
*   Exercise #3
*   Description:
*   Dado un arreglo de números desordenado y un entero k,
*   escribe una función que encuentre el k-ésimo elemento
*   más grande. Por ejemplo si el arreglo es [7, 2, 5, 9 ,8, 3]
*   y k es 2 debe regresar 8.
*   author: César Alberto Trejo Juárez
*
*/
const colors = require("colors");

const biggestNumberOnArray = (array, kNumber) => {
    if(Array.isArray(array)){
        let arrayTest = array;
        return arrayTest.sort((a, b) => b - a)[kNumber-1];
    } else {
        console.log("I'm sorry. Your input isn't an array.".red);
        throw new TypeError("I'm sorry your input is not an array.")
    }
}

module.exports = {
    biggestNumberOnArray: biggestNumberOnArray
};