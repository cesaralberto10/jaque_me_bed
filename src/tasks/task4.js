/*
*   Exercise #4
*   Description:
*   Escribe una función que reciba un arreglo de números
*   y quite los elementos duplicados
*   author: César Alberto Trejo Juárez
*
*/
const colors = require("colors");

const noDupplicateValues = (array) => {
    if( Array.isArray(array) ) return array.filter( (value, index, arr) => index === arr.indexOf(value) );
};

module.exports = {
    noDupplicateValues: noDupplicateValues
};