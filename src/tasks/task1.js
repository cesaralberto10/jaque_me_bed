/*
*   Exercise #1
*   Description:
*   Escribir una función que reciba un arreglo de números y obtenga
*   el subarreglo más grande en el que cada elemento del subarreglo
*   sea mayor que el anterior. Por ejemplo si el arreglo es
*   [3,2,5,9,1,3] tiene que regresar [2,5,9]
*   author: César Alberto Trejo Juárez
*
*/
const colors = require("colors");

const getSubArray = (arrayOfNumbers) => {
    // code here
    let tempList = [];
    let lists = [];
    arrayOfNumbers.forEach( (currentValue, index, array) => {
        tempList.push(currentValue);
        if(currentValue > array[index+1] || index == array.length -1 ){
            lists.push(tempList);
            tempList = [];
        }
    });
    return lists.sort( (subListA, subListB) => subListA.length - subListB.length)[lists.length - 1];
};

module.exports = {
    getSubArray: getSubArray
};