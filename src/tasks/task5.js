/*
*   Exercise #5
*   Description:
*   Dada una cadena de paréntesis y corchetes escribe una función
*   que regresa si la cadena está balanceada, es decir, por cada
*   paréntesis o corchete que abre hay uno al mismo nivel que cierra.   
*   author: César Alberto Trejo Juárez
*
*/
const checkBrackets = (stringWithBrackets) => {
    if(stringWithBrackets.length%2!==0) return false;
    var conditions = {
        "(": ( () => {return ')'} ),
        "[": ( () => {return ']'} ),
        "{": ( () => {return '}'} )
    };
    var stringLength = stringWithBrackets.length;
    var original = stringWithBrackets.split("").slice(0, stringWithBrackets.length/2);
    var reversed = stringWithBrackets.split("").reverse().slice(0, stringWithBrackets.length/2);
    for(let index = 0; index < stringLength/2; index++){
        if(reversed[index] !== conditions[`${original[index]}`]()) return false;
    }
    return true;
}

module.exports = {
    checkBrackets: checkBrackets
};