const { getSubArray } = require("./tasks/task1");
const { summation } = require("./tasks/task2");
const { biggestNumberOnArray } = require("./tasks/task3");
const { noDupplicateValues } = require("./tasks/task4");
const { checkBrackets } = require("./tasks/task5");

console.log("Iniciando...".bgMagenta);
console.log("Task1. Input: [1,2,1,9,8,2,4,6,7,8,10,2]");
let subArray = getSubArray([1,2,1,9,8,2,4,6,7,8,10,2]);
console.log(`Task1. Output: [${subArray}]`);

console.log("Task2. Input: n=7 & m=13");
let summationResult = summation(7, 13);
console.log(`Task2. Output: ${summationResult}`);

console.log("Task3. Input: [3,2,5,9,1,3] & k=3");
let numberResult = biggestNumberOnArray([3,2,5,9,1,3], 3);
console.log(`Task3. Output: ${numberResult}`);

console.log("Task4. Input: [1,2,1,9,8,2,4,6,7,8,10,2]");
let noDupplicateArray = noDupplicateValues([1,2,1,9,8,2,4,6,7,8,10,2]);
console.log(`Task4. Output: [${noDupplicateArray}]`);

console.log("Task5. Input: '[([{{[{[]}]}}])]'");
let balancedBrackets = checkBrackets('[([{{[{[]}]}}]))');
console.log(`Task5. Output: ${balancedBrackets}`);