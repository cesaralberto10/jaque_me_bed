const assert = require("assert");
const colors = require("colors");
const { biggestNumberOnArray } = require("../tasks/task3");
const { noDupplicateValues } = require("../tasks/task4");
const { summation } = require("../tasks/task2");
const { findMinClassrooms } = require("../tasks/task6");
const { getSubArray } = require("../tasks/task1");
const { checkBrackets } = require("../tasks/task5");


// describe('Array'.blue, function() {
//     describe('isArray()'.cyan, function() {
//         it('should return `false` when the input is not an array'.green, function() {
//             assert.notEqual(Array.isArray("Hello tester!"), true);
//         });
//     });
// });

describe('The biggest number on an array'.blue, () => {
    describe('biggestNumberOnArray()'.cyan, () => {
        it('should return 999 and 8'.green, () => {
            assert.equal(biggestNumberOnArray([0, 88, 888, '9999', 753, 56, 51, 3, 999, 18], 2), 999);
            assert.equal(biggestNumberOnArray([7,2,5,9,8,3], 2), 8);
        });
    });
});

describe('No duplicate values on an array'.blue, () => {
    describe('noDuplicateValues()'.cyan, () => {
        it('should return an array without duplicate elements'.green, () => {
            assert.deepEqual(noDupplicateValues([1,6,1,8,85,6,999,999,999,7,200,900,350,350,345,1675]), [1,6,8,85,999,7,200,900,350,345,1675]);
        });
    });
});

describe('Summation '.blue, () => {
    describe('summation(n, m)'.cyan, () => {
        it('should return a summation from n to m'.green, () => {
            assert.equal(summation(3,6), 18);
            assert.equal(summation(5,10), 45);
            assert.equal(summation(1,999), 499500);
        });
    });
});

describe('Sub-array'.blue, () => {
    describe('getSubArray(arrayOfNumbers)'.cyan, ()=> {
        it('should return a sub-array with the biggest number sequence'.green, () => {
            assert.deepEqual(getSubArray([3,2,5,9,1,3]), [2,5,9]);
        });
    });
});

describe("Brackets".blue, () => {
    describe("checkBrackets(stringOfBrackets)".cyan, () => {
        it('should return true when brackets are balanced'.green, () => {
            assert.equal(checkBrackets("[{(}]"), false);
            assert.equal(checkBrackets("[{()}]"), true);
            assert.equal(checkBrackets("{[()]}"), true);
            assert.equal(checkBrackets("{[[(])]}"), false);
            assert.equal(checkBrackets("{[()]}"), true);
            assert.equal(checkBrackets("[{()]"), false);
        });
    });
});

// describe('Schedule '.blue, () => {
//     describe('findMinClassrooms(scheduleList)'.cyan, () => {
//         it('should return the min of classrooms'.green, () => {
//             assert.equal(findMinClassrooms([
//                 {start: '11:00', end: '14:00'},
//                 {start: '12:00', end: '15:00'},
//                 {start: '14:30', end: '16:00'}
//             ]), 2);
//         });
//     });
// });